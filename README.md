# Gothime Manager

## Lancer l'application en local

Crée le build du front, du back, de la base de données et lancer le container
```
docker-compose up --build
```

Se connecter à l'adresse locale http://localhost:8080/

## Lancer l'application en ligne

Se connecter à l'adresse en ligne http://178.62.32.228:8080/

## Comptes utilisateur

### Utilisateur Admin

- Email : admin@gmail.com
- Password : password


### Utilisateur Manager

- Email : manager@gmail.com
- Password : password

### Utilisateur Employé

- Email : bastien.angles@gmail.com
- Password : password

- Email : victor.mader@gmail.com
- Password : password

- Email : theo.capelle@gmail.com
- Password : password