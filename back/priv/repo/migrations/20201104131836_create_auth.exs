defmodule ApiProject.Repo.Migrations.CreateAuth do
  use Ecto.Migration

  def change do
    create table(:auth) do
      add :type, :string
      add :token, :text
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

  end
end
