defmodule ApiProject.Repo.Migrations.CreateTeams do
  use Ecto.Migration

  def change do
    create table(:teams) do
      add :name, :string
      add :description, :string
      add :manager_id, references(:users, on_delete: :nothing), null: false

      timestamps()
    end

  end
end
