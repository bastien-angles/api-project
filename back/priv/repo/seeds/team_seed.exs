alias ApiProject.Repo
alias ApiProject.Teams.Team
alias ApiProject.TeamUsers.TeamUser

Repo.insert! %Team{
  name: "Equipe 1",
  description: "",
  manager_id: 2
}

Repo.insert! %Team{
  name: "Equipe 2",
  description: "",
  manager_id: 3
}

Repo.insert! %TeamUser{
  team_id: 1,
  user_id: 4
}

Repo.insert! %TeamUser{
  team_id: 1,
  user_id: 5
}

Repo.insert! %TeamUser{
  team_id: 2,
  user_id: 5
}

Repo.insert! %TeamUser{
  team_id: 2,
  user_id: 6
}
