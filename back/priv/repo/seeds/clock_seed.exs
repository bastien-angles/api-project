alias ApiProject.Repo
alias ApiProject.Clocks.Clock

Repo.insert! %Clock{
  time: ~N[2020-10-20 07:00:00],
  status: false,
  workingtime_id: 1,
  user_id: 3
}

Repo.insert! %Clock{
  time: ~N[2020-10-21 01:30:00],
  status: false,
  workingtime_id: 2,
  user_id: 3
}

Repo.insert! %Clock{
  time: ~N[2020-10-22 02:00:00],
  status: false,
  workingtime_id: 3,
  user_id: 4
}

Repo.insert! %Clock{
  time: ~N[2020-10-23 04:00:00],
  status: false,
  workingtime_id: 4,
  user_id: 4
}

Repo.insert! %Clock{
  time: ~N[2020-10-23 06:00:00],
  status: false,
  workingtime_id: 5,
  user_id: 5
}

Repo.insert! %Clock{
  time: ~N[2020-10-23 06:00:00],
  status: false,
  workingtime_id: 6,
  user_id: 5
}
