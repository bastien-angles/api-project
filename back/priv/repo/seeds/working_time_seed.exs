alias ApiProject.Repo
alias ApiProject.WorkingTimes.WorkingTime

Repo.insert! %WorkingTime{
    start: ~N[2020-10-20 14:00:00],
    end: ~N[2020-10-20 21:00:00],
    user_id: 3
}

Repo.insert! %WorkingTime{
    start: ~N[2020-10-21 18:30:00],
    end: ~N[2020-10-21 20:00:00],
    user_id: 3
}

Repo.insert! %WorkingTime{
    start: ~N[2020-10-22 18:00:00],
    end: ~N[2020-10-22 20:00:00],
    user_id: 4
}

Repo.insert! %WorkingTime{
    start: ~N[2020-10-23 08:00:00],
    end: ~N[2020-10-23 12:00:00],
    user_id: 4
}

Repo.insert! %WorkingTime{
    start: ~N[2020-10-23 14:00:00],
    end: ~N[2020-10-23 20:00:00],
    user_id: 5
}

Repo.insert! %WorkingTime{
    start: ~N[2020-10-23 14:00:00],
    end: ~N[2020-10-23 20:00:00],
    user_id: 5
}
