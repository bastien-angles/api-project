alias ApiProject.Repo
alias ApiProject.Users.User

Repo.insert! %User{
    username: "Admin",
    email: "admin@gmail.com",
    password_hash: Bcrypt.hash_pwd_salt("password"),
    role_id: 1
}

Repo.insert! %User{
    username: "Manager",
    email: "manager@gmail.com",
    password_hash: Bcrypt.hash_pwd_salt("password"),
    role_id: 2
}

Repo.insert! %User{
    username: "Manageuse",
    email: "manageuse@gmail.com",
    password_hash: Bcrypt.hash_pwd_salt("password"),
    role_id: 2
}

Repo.insert! %User{
    username: "Bastien",
    email: "bastien.angles@gmail.com",
    password_hash: Bcrypt.hash_pwd_salt("password"),
    role_id: 3
}

Repo.insert! %User{
    username: "Victor",
    email: "victor.mader@gmail.com",
    password_hash: Bcrypt.hash_pwd_salt("password"),
    role_id: 3
}

Repo.insert! %User{
    username: "Théo",
    email: "theo.capelle@gmail.com",
    password_hash: Bcrypt.hash_pwd_salt("password"),
    role_id: 3
}
