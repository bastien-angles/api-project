alias ApiProject.Repo
alias ApiProject.Roles.Role

Repo.insert! %Role{
  name: "Admin",
  slug: "admin",
  description: "Manage all users and teams"
}

Repo.insert! %Role{
  name: "Manager",
  slug: "manager",
  description: "Manage employees and teams"
}

Repo.insert! %Role{
  name: "Employee",
  slug: "employee",
  description: "Basic user"
}
