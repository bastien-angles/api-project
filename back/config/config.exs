# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :api_project,
  ecto_repos: [ApiProject.Repo]

# Configures the endpoint
config :api_project, ApiProjectWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "U7+gEWywBHq82WX7feVUohg3wvF9cavSX+o+OINzHb1Af1W0c4QSpBpAYavxcCW/",
  render_errors: [view: ApiProjectWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ApiProject.PubSub,
  live_view: [signing_salt: "KXl8wVUA"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Config Guardian
config :api_project, 
  ApiProject.Guardian,
  issuer: "api_project",
  secret_key: "QBROGkmIZbsua8+1Os6rPhc0uhuOjiSJtzw0v42gFTrfMEDRLtwa/L+DtRy3BKvd"