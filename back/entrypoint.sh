#!/bin/bash
# Docker entrypoint script.

# Wait until Postgres is ready

# Create, migrate, and seed database if it doesn't exist.
mix ecto.create
mix ecto.migrate
mix run priv/repo/seeds/role_seed.exs
mix run priv/repo/seeds/user_seed.exs
mix run priv/repo/seeds/team_seed.exs
mix run priv/repo/seeds/working_time_seed.exs
mix run priv/repo/seeds/clock_seed.exs
mix phx.server