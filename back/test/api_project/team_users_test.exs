defmodule ApiProject.TeamUsersTest do
  use ApiProject.DataCase

  alias ApiProject.TeamUsers

  describe "team_users" do
    alias ApiProject.TeamUsers.TeamUser

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def team_user_fixture(attrs \\ %{}) do
      {:ok, team_user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> TeamUsers.create_team_user()

      team_user
    end

    test "list_team_users/0 returns all team_users" do
      team_user = team_user_fixture()
      assert TeamUsers.list_team_users() == [team_user]
    end

    test "get_team_user!/1 returns the team_user with given id" do
      team_user = team_user_fixture()
      assert TeamUsers.get_team_user!(team_user.id) == team_user
    end

    test "create_team_user/1 with valid data creates a team_user" do
      assert {:ok, %TeamUser{} = team_user} = TeamUsers.create_team_user(@valid_attrs)
      assert team_user.age == 42
      assert team_user.name == "some name"
    end

    test "create_team_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = TeamUsers.create_team_user(@invalid_attrs)
    end

    test "update_team_user/2 with valid data updates the team_user" do
      team_user = team_user_fixture()
      assert {:ok, %TeamUser{} = team_user} = TeamUsers.update_team_user(team_user, @update_attrs)
      assert team_user.age == 43
      assert team_user.name == "some updated name"
    end

    test "update_team_user/2 with invalid data returns error changeset" do
      team_user = team_user_fixture()
      assert {:error, %Ecto.Changeset{}} = TeamUsers.update_team_user(team_user, @invalid_attrs)
      assert team_user == TeamUsers.get_team_user!(team_user.id)
    end

    test "delete_team_user/1 deletes the team_user" do
      team_user = team_user_fixture()
      assert {:ok, %TeamUser{}} = TeamUsers.delete_team_user(team_user)
      assert_raise Ecto.NoResultsError, fn -> TeamUsers.get_team_user!(team_user.id) end
    end

    test "change_team_user/1 returns a team_user changeset" do
      team_user = team_user_fixture()
      assert %Ecto.Changeset{} = TeamUsers.change_team_user(team_user)
    end
  end
end
