defmodule ApiProjectWeb.ClockControllerTest do
  use ApiProjectWeb.ConnCase

  alias ApiProject.Users
  alias ApiProject.Clocks
  alias ApiProject.Clocks.Clock

  @create_attrs %{
    status: true,
    time: ~N[2010-04-17 14:00:00]
  }
  @update_attrs %{
    status: false,
    time: ~N[2011-05-18 15:01:01]
  }
  @create_attrs_user %{
    email: "some email",
    username: "some username",
    password: "some password"
  }
  @invalid_attrs %{status: nil, time: nil}

  def fixture(:clock,:user) do
    {:ok, clock} = Clocks.create_clock(@create_attrs)
    clock
  end

  # MS BAILLE
  # defp create_user(_) do
  #   user = fixtureUser(:user)
  #   %{user: user}
  # end

  # def fixtureUser(:user) do
  #   {:ok, user} = Users.create_user(@create_attrs_user)
  #   user
  # end

  # FIN DE MES BAILLES

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  @doc """
  describe "index" do
    test "lists all clocks", %{conn: conn} do
      conn = get(conn, Routes.clock_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  """


  describe "create clock" do


    test "renders clock when data is valid", %{conn: conn} do

      user = Users.create_user(@create_attrs_user)
      IO.puts("++++++#{inspect(user)}")
      conn = post(conn, Routes.clock_path(conn, :create, user) ,  %{
        status: true,
        time: ~N[2010-04-17 14:00:00],
        user: user.id
      })

      IO.puts("++++++++++++++++++++#{inspect(conn)}")
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.clock_patch(conn, :show))

      assert %{
               "id" => id,
               "status" => true,
               "time" => "2010-04-17T14:00:00"
             } = hd(json_response(conn, 200)["data"])
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.clock_patch(conn, :create), @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  @doc """
  describe "update clock" do
    setup [:create_clock]

    test "renders clock when data is valid", %{conn: conn, clock: %Clock{id: id} = clock} do
      conn = put(conn, Routes.clock_path(conn, :update, clock), clock: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.clock_path(conn, :show, id))

      assert %{
               "id" => id,
               "status" => false,
               "time" => "2011-05-18T15:01:01"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, clock: clock} do
      conn = put(conn, Routes.clock_path(conn, :update, clock), clock: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete clock" do
    setup [:create_clock]

    test "deletes chosen clock", %{conn: conn, clock: clock} do
      conn = delete(conn, Routes.clock_path(conn, :delete, clock))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.clock_path(conn, :show, clock))
      end
    end
  end

  defp create_clock(_) do
    clock = fixture(:clock)
    %{clock: clock}
  end
  """

end
