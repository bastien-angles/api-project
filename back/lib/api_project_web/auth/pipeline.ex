defmodule ApiProjectWeb.AuthPipeline do
  require Logger

  alias ApiProject.Auths

  use Guardian.Plug.Pipeline, otp_app: :ApiProjectWeb,
  module: ApiProject.Guardian,
  error_handler: ApiProjectWeb.AuthErrorHandler

  plug :check_csrf
  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource

  def check_csrf(conn, opts) do
    case get_req_header(conn, "authorization") do
      [token] ->

        # Remove prefix Bearer
        token = String.slice(token, 7..-1) 

        # Get aes_256_key used before for encrypt this jwt
        aes_256_key = Auths.get_aes_256_key_by_jwt(token)
        # Get initialization_vector used before for encrypt this jwt
        init_vec = Auths.get_init_vec_by_jwt(token)

        # Checks token validity
        if (!is_nil(aes_256_key) && !is_nil(init_vec)) do
        
          # Decode from base 64
          {:ok, aes_256_key} = Base.decode64(aes_256_key.token)
          {:ok, init_vec} = Base.decode64(init_vec.token)
          {:ok, jwt} = Base.decode64(token)

          {:ok, jwt} = ExCrypto.decrypt(aes_256_key, init_vec, jwt)
      
          # Check if encrypted csrf token matches with csrf in headers
          case get_req_header(conn, "x-csrf-token") do
            [csrf_token] ->
            
              # Set uncryped jwt in header for next guardian checks
              conn = Plug.Conn.put_req_header(conn, "authorization", "Bearer " <> jwt)

              {:ok, jwt} = ApiProject.Guardian.decode_and_verify(jwt)
              if csrf_token == jwt["csrf"] do
                conn
              else
                ApiProjectWeb.AuthErrorHandler.auth_error(conn, {"invalid_csrf_token", "tokens not match"}, opts)
              end
            _ -> 
              ApiProjectWeb.AuthErrorHandler.auth_error(conn, {"invalid_csrf_token", "tokens not match"}, opts)
          end

        else
          ApiProjectWeb.AuthErrorHandler.auth_error(conn, {"invalid_auth_token", "invalid tokens"}, opts)
        end
      _ -> 
        conn
    end
  end
end