defmodule ApiProjectWeb.Router do
  use ApiProjectWeb, :router
  alias ApiProjectWeb.AuthPipeline

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :authenticated do
    plug AuthPipeline
  end

  scope "/", ApiProjectWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", ApiProjectWeb do
    pipe_through :api

    # User Routes
    post "/sign_in", UserController, :signIn
    post "/sign_up", UserController, :create
  end

  scope "/api", ApiProjectWeb do
    pipe_through [:api, :authenticated]

    # User Routes
    get "/users", UserController, :showByEmail
    get "/sign_out/:user_id", UserController, :signOut
    get "/users/all", UserController, :showAll
    get "/users/team/:team_id", UserController, :showAllByTeam
    resources "/users", UserController, only: [:show, :create, :update, :delete]

    # Working Time Routes
    get "/workingtimes/:user_id/:id", WorkingTimeController, :show
    get "/workingtimes/:user_id", WorkingTimeController, :showAll
    post "/workingtimes/:user_id", WorkingTimeController, :create
    resources "/workingtimes", WorkingTimeController, only: [:show, :update, :delete]

    # Clocking Routes
    get "/clocks/:user_id", ClockController, :show
    post "/clocks/:user_id", ClockController, :create
    resources "/clocks", ClockController, only: [:update, :delete]

    # Teams Routes
    # get "/teams/:user_id/:id", TeamController, :showTeam
    get "/teams/all", TeamController, :index
    get "/teams/manager/:manager_id", TeamController, :showAllTeamsByManager
    get "/teams/user/:user_id", TeamController, :showAllTeamsByUser
    post "/teams/add-user", TeamController, :addMember
    delete "/teams/remove-user/:team_id/:user_id", TeamController, :removeMember
    resources "/teams", TeamController, only: [:show, :update, :delete]

  end

  # Other scopes may use custom stacks.
  # scope "/api", ApiProjectWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: ApiProjectWeb.Telemetry
    end
  end
end
