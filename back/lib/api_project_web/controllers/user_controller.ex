defmodule ApiProjectWeb.UserController do
  use ApiProjectWeb, :controller

  require Logger
  import Ecto.Query
  alias ApiProject.Repo
  alias ApiProject.Users
  alias ApiProject.Auths
  alias ApiProject.Users.User
  alias ApiProject.Teams.Team
  alias ApiProject.TeamUsers.TeamUser
  alias ApiProject.Guardian

  action_fallback ApiProjectWeb.FallbackController

  def signIn(conn, %{"email" => email, "password" => password}) do
    case Users.token_sign_in(email, password) do
      {:ok, aes_256_key, init_vec, jwt, csrf, user} ->

        conn
        |> put_status(:ok)
        |> render("jwt.json", %{jwt: jwt, csrf: csrf, user: Poison.encode!(user)})
      _ ->
        {:error, :unauthorized, "These email and password doesn't match any account."}
    end
  end

  def signOut(conn, %{"user_id" => user_id}) do
    # Delete auth tokens for this users
    Auths.delete_auth_of_user(user_id)
    conn
    |> send_resp(:ok, "")
  end

  def index(conn, _params) do
    users = Users.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Users.create_user(user_params),
      {:ok, aes_256_key, init_vec, jwt, csrf, user} <- Users.token_sign_up(user)
    do
      conn
      |> put_session(:aes_256_key, aes_256_key)
      |> put_session(:init_vec, init_vec)
      |> put_status(:ok)
      |> render("jwt.json", %{jwt: jwt, csrf: csrf, user: Poison.encode!(user)})
    end
  end

  def show(conn, %{"id" => id}) do
    user = Users.get_user!(id) |> Repo.preload(:role)
    render(conn, "show.json", user: user)
  end

  def showByEmail(conn, %{"email" => email, "username" => username}) do
    user = User
    |> QueryBuilder.where(username: username)
    |> QueryBuilder.where(email: email)
    |> Repo.one()
    |> Repo.preload(:role)
    render(conn, "show.json", user: user)
  end

  def showAll(conn, _params) do
    users = Users.list_users() |> Repo.preload(:role)
    render(conn, "index.json", users: users)
  end

  def showAllByTeam(conn, %{"team_id" => team_id}) do
    query = from u in User,
        left_join: tu in TeamUser,
        on: u.id == tu.user_id,
        left_join: t in Team,
        on: tu.team_id == t.id,
        where: t.id == ^team_id,
        select: %{id: u.id, username: u.username, email: u.email, password_hash: u.password_hash, role: u.role_id}

    users = Repo.all(query)
    render(conn, "index.json", users: users)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Users.get_user!(id) |> Repo.preload(:role)

    with {:ok, %User{} = user} <- Users.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Users.get_user!(id) |> Repo.preload(:role)

    with {:ok, %User{}} <- Users.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
