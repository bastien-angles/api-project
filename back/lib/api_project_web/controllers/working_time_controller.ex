defmodule ApiProjectWeb.WorkingTimeController do
  require Logger
  import Ecto.Query
  use ApiProjectWeb, :controller

  alias ApiProject.Repo
  alias ApiProject.WorkingTimes
  alias ApiProject.WorkingTimes.WorkingTime

  action_fallback ApiProjectWeb.FallbackController

  def index(conn, _params) do
    workingtimes = WorkingTimes.list_workingtimes()
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def create(conn, %{"working_time" => working_time_params}) do
    with {:ok, %WorkingTime{} = working_time} <- WorkingTimes.create_working_time(working_time_params) do
      # Preload relations 
      working_time = working_time 
                    |> Repo.preload(:user)
                    |> Repo.preload(user: :role)
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.working_time_path(conn, :show, working_time))
      |> render("show.json", working_time: working_time)
      end
  end

  def show(conn, %{"user_id" => _user_id, "id" => id}) do
    working_time = WorkingTimes.get_working_time!(id) 
                  |> Repo.preload(:user)
                  |> Repo.preload(user: :role)
    render(conn, "show.json", working_time: working_time)
  end

  def showAll(conn, params) do
    # Check if user params is set and it's an integer
    unless Regex.match?(~r{\A\d*\z}, params["user_id"]) do
      raise ArgumentError, message: "'user' param is required and must be a integer"
    end

    query = cond do
      Map.has_key?(params, "start") and Map.has_key?(params, "end") ->
        from wt in WorkingTime, where: wt.user_id == ^params["user_id"] and
        (
          (wt.start >= ^params["start"] and wt.end >= ^params["end"] and wt.start < ^params["end"]) or
          (wt.start <= ^params["start"] and wt.end <= ^params["end"] and wt.end > ^params["start"]) or
          (wt.start <= ^params["start"] and wt.end >= ^params["end"]) or
          (wt.start >= ^params["start"] and wt.end <= ^params["end"])
        )

      Map.has_key?(params, "start") ->
        from wt in WorkingTime, where: wt.user_id == ^params["user_id"] and wt.start >= ^params["start"]

      Map.has_key?(params, "end") ->
        from wt in WorkingTime, where: wt.user_id == ^params["user_id"] and wt.end <= ^params["end"]

      true ->
        from wt in WorkingTime, where: wt.user_id == ^params["user_id"]
    end
    workingtimes = query
                  |> Repo.all()
                  |> Repo.preload(:user)
                  |> Repo.preload(user: :role)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def update(conn, %{"id" => id, "working_time" => working_time_params}) do
    working_time = WorkingTimes.get_working_time!(id)
                  |> Repo.preload(:user)
                  |> Repo.preload(user: :role)
    with {:ok, %WorkingTime{} = working_time} <- WorkingTimes.update_working_time(working_time, working_time_params) do
      render(conn, "show.json", working_time: working_time)
    end
  end

  def delete(conn, %{"id" => id}) do
    working_time = WorkingTimes.get_working_time!(id)
                  |> Repo.preload(:user)
                  |> Repo.preload(user: :role)
    with {:ok, %WorkingTime{}} <- WorkingTimes.delete_working_time(working_time) do
      send_resp(conn, :no_content, "")
    end
  end
end
