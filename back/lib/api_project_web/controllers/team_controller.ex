defmodule ApiProjectWeb.TeamController do
  use ApiProjectWeb, :controller

  import Ecto.Query
  alias ApiProject.Repo
  alias ApiProject.Teams
  alias ApiProject.Teams.Team
  alias ApiProject.Users.User
  alias ApiProject.TeamUsers
  alias ApiProject.TeamUsers.TeamUser

  action_fallback ApiProjectWeb.FallbackController

  def index(conn, _params) do
    teams = Teams.list_teams()
    render(conn, "index.json", teams: teams)
  end

  def create(conn, %{"team" => team_params}) do
    with {:ok, %Team{} = team} <- Teams.create_team(team_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.team_path(conn, :show, team))
      |> render("show.json", team: team)
    end
  end

  def show(conn, %{"id" => id}) do
    team = Teams.get_team!(id)
    render(conn, "show.json", team: team)
  end

  def showAllTeamsByManager(conn, %{"manager_id" => manager_id}) do
    query = from t in Team,
        where: t.manager_id == ^manager_id,
        select: %{id: t.id, name: t.name, description: t.description}

    teams = Repo.all(query)
    render(conn, "index.json", teams: teams)
  end

  def showAllTeamsByUser(conn, %{"user_id" => user_id}) do
    query = from t in Team,
        left_join: tu in TeamUser,
        on: t.id == tu.team_id,
        left_join: u in User,
        on: tu.user_id == u.id,
        where: u.id == ^user_id,
        select: %{id: t.id, name: t.name, description: t.description}

    teams = Repo.all(query)
    render(conn, "index.json", teams: teams)
  end

  def addMember(conn, %{"team_id" => team_id, "user_id" => user_id}) do
    # Check if user isn't already in this team
    query = from t in TeamUser,
            where: t.team_id == ^team_id and t.user_id == ^user_id,
            select: %{id: t.id}
    exists = Repo.one(query)

    # If not exists insert new user in this team
    if is_nil(exists) do
      with {:ok, %TeamUser{} = relation} <- TeamUsers.create_team_user(%{team_id: team_id, user_id: user_id}) do
        send_resp(conn, :created, "")
      end
    else
      send_resp(conn, :conflict, "")
    end
  end

  def removeMember(conn, %{"team_id" => team_id, "user_id" => user_id}) do
    from(t in TeamUser, where: t.team_id == ^team_id and t.user_id == ^user_id) |> Repo.delete_all
    send_resp(conn, :no_content, "")
  end

  def update(conn, %{"id" => id, "team" => team_params}) do
    team = Teams.get_team!(id)

    with {:ok, %Team{} = team} <- Teams.update_team(team, team_params) do
      render(conn, "show.json", team: team)
    end
  end

  def delete(conn, %{"id" => id}) do
    team = Teams.get_team!(id)

    with {:ok, %Team{}} <- Teams.delete_team(team) do
      send_resp(conn, :no_content, "")
    end
  end
end
