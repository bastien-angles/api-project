defmodule ApiProject.Users.User do
  use Ecto.Schema
  use QueryBuilder
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:username, :email, :role]}
  schema "users" do
    field :username, :string
    field :email, :string
    field :password_hash, :string
    # Virtual fields:
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    # Relationship
    belongs_to :role, ApiProject.Roles.Role
    many_to_many :teams, ApiProject.Teams.Team, join_through: ApiProject.TeamUsers.TeamUser


    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password, :password_confirmation, :role_id])
    |> validate_required([:username, :email, :role_id])
    |> validate_format(:email, ~r/@/) # Check that email is valid
    |> validate_length(:password, min: 8) # Check that password length is >= 8
    |> validate_confirmation(:password) # Check that password === password_confirmation
    |> unique_constraint(:email)
    |> put_password_hash # Add put_password_hash to changeset pipeline
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}}
        ->
          put_change(changeset, :password_hash, Bcrypt.hash_pwd_salt(pass))
      _ ->
          changeset
    end
  end
end
