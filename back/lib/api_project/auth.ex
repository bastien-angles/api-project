defmodule ApiProject.Auths do
  @moduledoc """
  The Auths context.
  """

  import Ecto.Query, warn: false
  alias ApiProject.Repo

  alias ApiProject.Auths.Auth

  @doc """
  Returns the list of auths.

  ## Examples

      iex> list_auths()
      [%Auth{}, ...]

  """
  def list_auths do
    Repo.all(Auth)
  end

  @doc """
  Gets a single auth.

  Raises `Ecto.NoResultsError` if the Auth does not exist.

  ## Examples

      iex> get_auth!(123)
      %Auth{}

      iex> get_auth!(456)
      ** (Ecto.NoResultsError)

  """
  def get_auth!(id), do: Repo.get!(Auth, id)

  @doc """
  Gets a single auth by user and type.

  Raises `Ecto.NoResultsError` if the Auth does not exist.
  """
  def get_auth_by_user_type(user_id, type) do
    query = from a in Auth,
          where: a.type == ^type and a.user_id == ^user_id,
          select: %{id: a.id, type: a.type, token: a.token, user_id: a.user_id}

    Repo.one(query)
  end

  @doc """
  Gets a single auth by jwt and type.

  Raises `Ecto.NoResultsError` if the Auth does not exist.
  """
  def get_auth_by_jwt_type(jwt, type) do
    query = from a1 in Auth,
          left_join: a2 in Auth,
          on: a1.user_id == a2.user_id,
          where: a1.type == ^type and a2.type == "jwt" and a2.token == ^jwt,
          select: %{id: a1.id, type: a1.type, token: a1.token, user_id: a1.user_id}

    Repo.one(query)
  end

  @doc """
  Gets a single auth of init_vec type by jwt.

  Raises `Ecto.NoResultsError` if the Auth does not exist.
  """
  def get_init_vec_by_jwt(jwt) do
    get_auth_by_jwt_type(jwt, "init_vec")
  end

  @doc """
  Gets a single auth of aes_256_key type by jwt.

  Raises `Ecto.NoResultsError` if the Auth does not exist.
  """
  def get_aes_256_key_by_jwt(jwt) do
    get_auth_by_jwt_type(jwt, "aes_256_key")
  end

  @doc """
  Creates a auth.

  ## Examples

      iex> create_auth(%{field: value})
      {:ok, %Auth{}}

      iex> create_auth(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_auth(attrs \\ %{}) do
    %Auth{}
    |> Auth.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a auth.

  ## Examples

      iex> update_auth(auth, %{field: new_value})
      {:ok, %Auth{}}

      iex> update_auth(auth, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_auth(%Auth{} = auth, attrs) do
    auth
    |> Auth.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a auth.

  ## Examples

      iex> delete_auth(auth)
      {:ok, %Auth{}}

      iex> delete_auth(auth)
      {:error, %Ecto.Changeset{}}

  """
  def delete_auth(%Auth{} = auth) do
    Repo.delete(auth)
  end

  @doc """
  Deletes all auth for a user
  """
  def delete_auth_of_user(user_id) do
    from(a in Auth, where: a.user_id == ^user_id) |> Repo.delete_all
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking auth changes.

  ## Examples

      iex> change_auth(auth)
      %Ecto.Changeset{data: %Auth{}}

  """
  def change_auth(%Auth{} = auth, attrs \\ %{}) do
    Auth.changeset(auth, attrs)
  end
end
