defmodule ApiProject.Clocks.Clock do
  use Ecto.Schema
  use QueryBuilder
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:time, :status, :workingtime, :user]}
  schema "clocks" do
    field :time, :naive_datetime
    field :status, :boolean, default: false
    # Relationship
    belongs_to :workingtime, ApiProject.WorkingTimes.WorkingTime
    belongs_to :user, ApiProject.Users.User

    timestamps()
  end

  @doc false
  def changeset(clock, attrs) do
    clock
    |> cast(attrs, [:time, :status, :user_id])
    |> validate_required([:time, :status, :user_id])
  end
end
