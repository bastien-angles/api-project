defmodule ApiProject.Auths.Auth do
  use Ecto.Schema
  import Ecto.Changeset

  schema "auth" do
    field :type, :string
    field :token, :string
    # Relationship
    belongs_to :user, ApiProject.Users.User

    timestamps()
  end

  @doc false
  def changeset(auth, attrs) do
    auth
    |> cast(attrs, [:type, :token, :user_id])
    |> validate_required([:type, :token, :user_id])
  end
end
