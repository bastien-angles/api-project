defmodule ApiProject.Teams.Team do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:name, :description, :manager]}
  schema "teams" do
    field :name, :string
    field :description, :string
    # Relationship
    belongs_to :manager, ApiProject.Users.User
    many_to_many :users, ApiProject.Users.User, join_through: ApiProject.TeamUsers.TeamUser

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:name, :description, :manager_id])
    |> validate_required([:name])
  end
end
