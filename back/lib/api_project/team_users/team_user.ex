defmodule ApiProject.TeamUsers.TeamUser do
  use Ecto.Schema
  use QueryBuilder
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:team, :user]}
  schema "team_users" do
    # Relationship
    belongs_to :team, ApiProject.Teams.Team
    belongs_to :user, ApiProject.Users.User

    timestamps()
  end

  @doc false
  def changeset(team_user, attrs) do
    team_user
    |> cast(attrs, [:team_id, :user_id])
    |> validate_required([:team_id, :user_id])
  end
end
