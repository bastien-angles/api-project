defmodule ApiProject.WorkingTimes.WorkingTime do
  use Ecto.Schema
  use QueryBuilder
  import Ecto.Changeset
  
  @derive {Jason.Encoder, only: [:end, :start, :user]}
  schema "workingtimes" do
    field :end, :naive_datetime
    field :start, :naive_datetime
    # Relationship
    belongs_to :user, ApiProject.Users.User

    timestamps()
  end

  @doc false
  def changeset(working_time, attrs) do
    working_time
    |> cast(attrs, [:start, :end, :user_id])
    |> validate_required([:start, :end, :user_id])
  end
end
