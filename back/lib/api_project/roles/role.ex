defmodule ApiProject.Roles.Role do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:name, :description, :slug]}
  schema "roles" do
    field :name, :string
    field :slug, :string
    field :description, :string

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:name, :description, :slug])
    |> validate_required([:name])
  end
end
