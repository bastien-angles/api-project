defmodule ApiProject.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias ApiProject.Repo
  alias ApiProject.Guardian
  alias ApiProject.Users.User
  alias ApiProject.Auths
  alias ApiProject.Auths.Auth
  alias ApiProject.Roles

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    # Put employee role by default
    employee = Roles.get_role_by_slug!("employee")
    attrs = attrs |> Map.put("role_id", employee.id)
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  
  #Returns an user with a given email input.
  #
  #If user is not found, return dummy check. 
  #It seem to a client like a process is running to check a password, 
  #usefull when someone attempts to log in with an unregistered email so that user enumeration by bad actors will be more difficult. 
  defp get_by_email(email) when is_binary(email) do
    case Repo.get_by(User, email: email) |> Repo.preload(:role) do
      nil ->
        Bcrypt.no_user_verify()
        {:error, "Login error."}
      user ->
        {:ok, user}
    end
  end

  #Check user input password against encrypted hash saved in DB.
  defp verify_password(password, %User{} = user) when is_binary(password) do
    if Bcrypt.verify_pass(password, user.password_hash) do
      {:ok, user}
    else
      {:error, :invalid_password}
    end
  end

  #Check if email user exists then check if password matches
  defp email_password_auth(email, password) when is_binary(email) and is_binary(password) do
    with {:ok, user} <- get_by_email(email),
    do: verify_password(password, user)
  end

  @doc """
  Check email / password inputs and create token if match
  """
  def token_sign_in(email, password) do
    case email_password_auth(email, password) do
      {:ok, user} ->

        # Get CSRF for this user from database or generate new and save
        auth_csrf = Auths.get_auth_by_user_type(user.id, "csrf")
        csrf = if is_nil(auth_csrf) do
          csrf = Plug.CSRFProtection.get_csrf_token
          Auths.create_auth(%{type: "csrf", token: csrf, user_id: user.id})
          csrf
        else
          auth_csrf.token
        end

        with {:ok, token, _claims} <- Guardian.encode_and_sign(user, %{csrf: csrf}) do

          # Get AES_256_KEY for this user from database or generate new and save
          auth_aes_256_key = Auths.get_auth_by_user_type(user.id, "aes_256_key")
          aes_256_key = if is_nil(auth_aes_256_key) do
                          {:ok, aes_256_key} = ExCrypto.generate_aes_key(:aes_256, :bytes)
                          Auths.create_auth(%{type: "aes_256_key", token: Base.encode64(aes_256_key), user_id: user.id})
                          aes_256_key
                        else
                          auth_aes_256_key.token
                        end
          
          # Get INIT_VEC for this user from database or generate new and save
          auth_init_vec = Auths.get_auth_by_user_type(user.id, "init_vec")
          iv_jwt = if is_nil(auth_init_vec) do
                    {:ok, {init_vec, jwt}} = ExCrypto.encrypt(aes_256_key, token)
                    
                    Auths.create_auth(%{type: "init_vec", token: Base.encode64(init_vec), user_id: user.id})
                    {:ok, %Auth{} = jwt} = Auths.create_auth(%{type: "jwt", token: Base.encode64(jwt), user_id: user.id})
                    
                    %{init_vec: init_vec, jwt: jwt.token}
                  else
                    auth_jwt = Auths.get_auth_by_user_type(user.id, "jwt")
                    %{init_vec: auth_init_vec, jwt: auth_jwt.token}
                  end

          # Send tokens to client request
          {:ok, aes_256_key, iv_jwt.init_vec, iv_jwt.jwt, csrf, user}
        end
      _ ->
        {:error, :unauthorized}
    end
  end

  @doc """
  Create token for new user
  """
  def token_sign_up(user) do
      # Create CSRF for this user and save in DB
      csrf = Plug.CSRFProtection.get_csrf_token
      Auths.create_auth(%{type: "csrf", token: csrf, user_id: user.id})

      with {:ok, token, _claims} <- Guardian.encode_and_sign(user, %{csrf: csrf}) do
        # Create AES_256_KEY for this user and save in DB
        {:ok, aes_256_key} = ExCrypto.generate_aes_key(:aes_256, :bytes)
        Auths.create_auth(%{type: "aes_256_key", token: Base.encode64(aes_256_key), user_id: user.id})

        # Get INIT_VEC for this user and save in DB
        {:ok, {init_vec, jwt}} = ExCrypto.encrypt(aes_256_key, token)
        Auths.create_auth(%{type: "init_vec", token: Base.encode64(init_vec), user_id: user.id})
        {:ok, %Auth{} = jwt} = Auths.create_auth(%{type: "jwt", token: Base.encode64(jwt), user_id: user.id})

        # Must to encode binary before send to client
        {:ok, aes_256_key, init_vec, jwt.token, csrf, user}
      end
  end

end
