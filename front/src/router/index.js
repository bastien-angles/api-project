import Vue from 'vue'
import VueRouter from 'vue-router'
import User from '../components/User.vue'
import Home from '../components/Home.vue'
import WorkingTimes from '../components/WorkingTimes.vue'
import Clocks from '../components/Clocks.vue'
import WorkingTime from '../components/WorkingTime.vue'
import ClockManager from '../components/ClockManager.vue'
import ChartManager from '../components/ChartManager.vue'
import AdminManager from '../components/AdminManager.vue'
import Auth from '../components/Auth.vue'
import Faq from '../components/Faq.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/user',
    name: 'User',
    component: User
  },
  {
    path: '/working-times/:userId',
    name: 'WorkingTimes',
    component: WorkingTimes
  },
  {
    path: '/clocks',
    name: 'Clocks',
    component: Clocks
  },
  {
    path: '/faq',
    name: 'Faq',
    component: Faq
  },
  {
    path: '/working-time',
    name: 'WorkingTime',
    component: WorkingTime
  },
  {
    path: '/clock-manager/:userId',
    name: 'ClockManager',
    component: ClockManager
  },
  {
    path: '/chart-manager/:userId',
    name: 'ChartManager',
    component: ChartManager
  },
  {
    path: '/admin-manager/:userId',
    name: 'AdminManager',
    component: AdminManager
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  document.title = 'Gothime Manager - Save time, fight crime !';
  next();
})

export default router
