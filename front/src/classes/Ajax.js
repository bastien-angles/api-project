import axios from 'axios'

export default class Ajax {
    
    /**
     * POST Request with Axios with JWT and X-CSRF-Token in header
     * @param {String} url 
     * @param {String} params 
     * @returns {Promise}
     */
    static post(url, params) {
        
        // Add header if exists
        var csrf = localStorage.csrf;
        var jwt = localStorage.jwt;
        var header = {};
        if (csrf && jwt) {
            header = {
                'Authorization': 'Bearer '+jwt,
                'X-Csrf-token': csrf
            }
        }
        return new Promise((resolve, reject) => {
            
            const instance = axios.create({
                baseURL: process.env.VUE_APP_API_URL,
                headers: header,
                timeout: 1000,
                withCredentials: true   // required to send session_id to elixir !!!!!!!!!!!!!!!!!!!!!! (6h)
            });
    
            instance.post(url, params)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    if (error.response.status == 401) {
                        localStorage.clear();
                        document.location.href="/";
                    }
                    reject(error);
                })
        });
    }

    /**
     * GET Request with Axios with JWT and X-CSRF-Token in header
     * @param {String} url 
     * @returns {Promise}
     */
    static get(url) {

        // Add header if exists
        var csrf = localStorage.csrf;
        var jwt = localStorage.jwt;
        var header = {};
        if (csrf && jwt) {
            header = {
                'Authorization': 'Bearer '+jwt,
                'X-Csrf-Token': csrf
            }
        }

        return new Promise((resolve, reject) => {
            const instance = axios.create({
                baseURL: process.env.VUE_APP_API_URL,
                headers: header,
                timeout: 1000,
                withCredentials: true   // required to send session_id to elixir !!!!!!!!!!!!!!!!!!!!!! (6h)
            });
            instance.get(url)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    if (error.response.status == 401) {
                        localStorage.clear();
                        document.location.href="/";
                    }
                    reject(error);
                })
        });
    }

    /**
     * PUT Request with Axios with JWT and X-CSRF-Token in header
     * @param {String} url 
     * @param {String} params 
     * @returns {Promise}
     */
    static put(url, params) {

        // Add header if exists
        var csrf = localStorage.csrf;
        var jwt = localStorage.jwt;
        var header = {};
        if (csrf && jwt) {
            header = {
                'Authorization': 'Bearer '+jwt,
                'X-Csrf-Token': csrf
            }
        }

        return new Promise((resolve, reject) => {
            
            const instance = axios.create({
                baseURL: process.env.VUE_APP_API_URL,
                headers: header,
                timeout: 1000,
                withCredentials: true   // required to send session_id to elixir !!!!!!!!!!!!!!!!!!!!!! (6h)
            });
    
            instance.put(url, params)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    if (error.response.status == 401) {
                        localStorage.clear();
                        document.location.href="/";
                    }
                    reject(error);
                })
        });
    }

    /**
     * DELETE Request with Axios with JWT and X-CSRF-Token in header
     * @param {String} url
     * @returns {Promise}
     */
    static delete(url) {

        // Add header if exists
        var csrf = localStorage.csrf;
        var jwt = localStorage.jwt;
        var header = {};
        if (csrf && jwt) {
            header = {
                'Authorization': 'Bearer '+jwt,
                'X-Csrf-Token': csrf
            }
        }

        return new Promise((resolve, reject) => {
            
            const instance = axios.create({
                baseURL: process.env.VUE_APP_API_URL,
                headers: header,
                timeout: 1000,
                withCredentials: true   // required to send session_id to elixir !!!!!!!!!!!!!!!!!!!!!! (6h)
            });
    
            instance.delete(url)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    if (error.response.status == 401) {
                        localStorage.clear();
                        document.location.href="/";
                    }
                    reject(error);
                })
        });
    }
}